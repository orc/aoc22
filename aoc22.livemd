<!-- livebook:{"persist_outputs":true} -->

# Advent of Code 2022 Day 1

## Day 1 Part 1

```elixir
defmodule Aoc do
  def read_file(fname) do
    {:ok, ftext} = File.read(fname)
    ftext
  end

  def find_max(x, {current_total, max_total}) do
    case x do
      x_trimmed when x_trimmed == "" ->
        if current_total > max_total do
          {0, current_total}
        else
          {0, max_total}
        end

      x_trimmed ->
        {current_total + String.to_integer(x_trimmed), max_total}
    end
  end

  def parse_file(ftext) do
    ftext
    |> String.split("\n")
    |> Enum.reduce({0, 0}, fn x, acc -> find_max(x, acc) end)
    |> elem(1)
  end
end

input =
  Aoc.read_file("advent2022-datasets/orc-01-01-dataset.txt")
  |> Aoc.parse_file()
```

<!-- livebook:{"output":true} -->

```
69289
```

## Day 1 Part 2

```elixir
defmodule Aoc do
  def read_file(fname) do
    {:ok, ftext} = File.read(fname)
    ftext
  end

  def compare_three(x, caloric_elves) do
    [x | caloric_elves]
    |> Enum.sort()
    |> Enum.reverse()
    |> Enum.take(3)
  end

  def find_max(x, {current_total, caloric_elves}) do
    case x do
      x_trimmed when x_trimmed == "" ->
        {0, compare_three(current_total, caloric_elves)}

      x_trimmed ->
        {current_total + String.to_integer(x_trimmed), caloric_elves}
    end
  end

  def parse_file(ftext) do
    ftext
    |> String.split("\n")
    |> Enum.reduce({0, [0, 0, 0]}, fn x, acc -> find_max(x, acc) end)
    |> elem(1)
    |> Enum.sum()
  end
end

input =
  Aoc.read_file("advent2022-datasets/orc-01-01-dataset.txt")
  |> Aoc.parse_file()
```

<!-- livebook:{"output":true} -->

```
205615
```

## Day 2 Part 1

```elixir
defmodule Aoc do
  @doc ~S"""
  A & X = Rock, 1 point
  B & Y = Paper, 2 points
  C & Z = Scissors, 3 points
  Loss = +0
  Tie = +3
  Win = +6
  """
  def read_file(fname) do
    {:ok, ftext} = File.read(fname)
    ftext
  end

  def decrypt() do
    %{
      "A X" => 4,
      "A Y" => 8,
      "A Z" => 3,
      "B X" => 1,
      "B Y" => 5,
      "B Z" => 9,
      "C X" => 7,
      "C Y" => 2,
      "C Z" => 6
    }
  end

  def parse_file(ftext) do
    ftext
    |> String.split("\n", trim: true)
    |> Enum.reduce(0, fn x, acc -> acc + (Map.fetch(decrypt(), x) |> elem(1)) end)
  end
end

Aoc.read_file("advent2022-datasets/orc-02-01-dataset.txt")
|> Aoc.parse_file()
```

<!-- livebook:{"output":true} -->

```
12458
```

## Day 2 Part 2

```elixir
defmodule Aoc do
  @doc ~S"""
  A = Rock, 1 point
  B = Paper, 2 points
  C = Scissors, 3 points
  X, need Loss = +0
  Y, need Tie = +3
  Z, need Win = +6
  """
  def read_file(fname) do
    {:ok, ftext} = File.read(fname)
    ftext
  end

  def decrypt() do
    # C
    %{
      "A X" => 3,
      # A
      "A Y" => 4,
      # B
      "A Z" => 8,
      # A
      "B X" => 1,
      # B
      "B Y" => 5,
      # C
      "B Z" => 9,
      # B
      "C X" => 2,
      # C
      "C Y" => 6,
      # A
      "C Z" => 7
    }
  end

  def parse_file(ftext) do
    ftext
    |> String.split("\n", trim: true)
    |> Enum.reduce(0, fn x, acc -> acc + (Map.fetch(decrypt(), x) |> elem(1)) end)
  end
end

Aoc.read_file("advent2022-datasets/orc-02-01-dataset.txt")
|> Aoc.parse_file()
```

<!-- livebook:{"output":true} -->

```
12683
```

## Day 3 Part 1

```elixir
defmodule Aoc do
  def read_file(fname) do
    {:ok, ftext} = File.read(fname)
    ftext
  end

  def shared_items(rucksack) do
    [left_compartment | right_compartment] =
      String.split_at(rucksack, div(String.length(rucksack), 2))
      |> Tuple.to_list()
      |> Enum.map(&String.split(&1, "", trim: true))
      |> Enum.map(&MapSet.new(&1))

    MapSet.intersection(left_compartment, List.first(right_compartment))
    |> MapSet.to_list()
  end

  def map_priority(item) do
    cond do
      97 <= item and item <= 122 ->
        item - 96

      65 <= item and item <= 90 ->
        item - 64 + 26
    end
  end

  def calc_priority(sack) do
    sack
    |> hd()
    |> String.to_charlist()
    |> Enum.map(&map_priority(&1))
  end

  def sum_priority(items) do
    items
    |> calc_priority()
    |> Enum.sum()
  end

  def parse_file(ftext) do
    ftext
    |> String.split("\n", trim: true)
  end
end

Aoc.read_file("advent2022-datasets/orc-03-01-dataset.txt")
|> Aoc.parse_file()
|> Enum.map(&Aoc.shared_items(&1))
|> Enum.map(&Aoc.sum_priority(&1))
|> Enum.sum()
```

<!-- livebook:{"output":true} -->

```
8088
```

## Day 3 Part 2

```elixir
defmodule Aoc do
  def read_file(fname) do
    {:ok, ftext} = File.read(fname)
    ftext
  end

  def shared_items(rucksack) do
    [left_compartment | right_compartment] =
      String.split_at(rucksack, div(String.length(rucksack), 2))
      |> Tuple.to_list()
      |> Enum.map(&String.split(&1, "", trim: true))
      |> Enum.map(&MapSet.new(&1))

    MapSet.intersection(left_compartment, List.first(right_compartment))
    |> MapSet.to_list()
  end

  def map_priority(charlist_item) do
    item = List.first(charlist_item)

    cond do
      97 <= item and item <= 122 ->
        item - 96

      65 <= item and item <= 90 ->
        item - 64 + 26

      true ->
        0
    end
  end

  def find_badge(group) do
    Enum.at(group, 0)
    |> MapSet.intersection(Enum.at(group, 1))
    |> MapSet.intersection(Enum.at(group, 2))
    |> MapSet.to_list()
    |> hd
    |> String.to_charlist()
  end

  def calc_priority(sack) do
    sack
    |> hd()
    |> String.to_charlist()
    |> Enum.map(&map_priority(&1))
  end

  def uniquify(sack) do
    sack
    |> String.split("", trim: true)
    |> MapSet.new()
  end

  def parse_file(ftext) do
    ftext
    |> String.split("\n", trim: true)
  end
end

Aoc.read_file("advent2022-datasets/orc-03-01-dataset.txt")
|> Aoc.parse_file()
|> Enum.map(&Aoc.uniquify(&1))
|> Enum.chunk_every(3)
|> Enum.map(&Aoc.find_badge(&1))
|> Enum.map(&Aoc.map_priority(&1))
|> Enum.sum()
```

<!-- livebook:{"output":true} -->

```
2522
```

## Day 4 Part 1

```elixir
defmodule Aoc do
  def read_file(fname) do
    {:ok, ftext} = File.read(fname)
    ftext
  end

  def create_ranges(line) do
    elves = String.split(line, ",")

    [assignment1, assignment2] =
      String.split(Enum.at(elves, 0), "-")
      |> Enum.map(&String.to_integer(&1))

    [assignment3, assignment4] =
      String.split(Enum.at(elves, 1), "-")
      |> Enum.map(&String.to_integer(&1))

    elf1 = assignment1..assignment2
    elf2 = assignment3..assignment4

    cond do
      assignment1 in elf2 and assignment2 in elf2 ->
        1

      assignment3 in elf1 and assignment4 in elf1 ->
        1

      true ->
        0
    end
  end

  def parse_file(ftext) do
    ftext
    |> String.split("\n", trim: true)
  end
end

Aoc.read_file("advent2022-datasets/orc-04-01-dataset.txt")
|> Aoc.parse_file()
|> Enum.reduce(0, fn x, acc -> acc + Aoc.create_ranges(x) end)
```

<!-- livebook:{"output":true} -->

```
571
```

## Day 4 Part 2

Day 4 part 1 becomes part 2 by switching the  `and` for an `or`.  That's basically the difference between one assignment being fully contained in the other versus one assignment overlapping another at all.

```elixir
defmodule Aoc do
  def read_file(fname) do
    {:ok, ftext} = File.read(fname)
    ftext
  end

  def create_ranges(line) do
    elves = String.split(line, ",")

    [assignment1, assignment2] =
      String.split(Enum.at(elves, 0), "-")
      |> Enum.map(&String.to_integer(&1))

    [assignment3, assignment4] =
      String.split(Enum.at(elves, 1), "-")
      |> Enum.map(&String.to_integer(&1))

    elf1 = assignment1..assignment2
    elf2 = assignment3..assignment4

    cond do
      assignment1 in elf2 or assignment2 in elf2 ->
        1

      assignment3 in elf1 or assignment4 in elf1 ->
        1

      true ->
        0
    end
  end

  def parse_file(ftext) do
    ftext
    |> String.split("\n", trim: true)
  end
end

Aoc.read_file("advent2022-datasets/orc-04-01-dataset.txt")
|> Aoc.parse_file()
|> Enum.reduce(0, fn x, acc -> acc + Aoc.create_ranges(x) end)
```

<!-- livebook:{"output":true} -->

```
917
```

## Day 5 Part 1

```elixir
defmodule Aoc do
  def read_file(fname) do
    {:ok, ftext} = File.read(fname)
    ftext
  end

  def create_ranges(line) do
    elves = String.split(line, ",")

    [assignment1, assignment2] =
      String.split(Enum.at(elves, 0), "-")
      |> Enum.map(&String.to_integer(&1))

    [assignment3, assignment4] =
      String.split(Enum.at(elves, 1), "-")
      |> Enum.map(&String.to_integer(&1))

    elf1 = assignment1..assignment2
    elf2 = assignment3..assignment4

    cond do
      assignment1 in elf2 or assignment2 in elf2 ->
        1

      assignment3 in elf1 or assignment4 in elf1 ->
        1

      true ->
        0
    end
  end

  def parse_stack(str) do
    String.slice(str, 1, 1) <>
      String.slice(str, 5, 1) <>
      String.slice(str, 9, 1) <>
      String.slice(str, 13, 1) <>
      String.slice(str, 17, 1) <>
      String.slice(str, 21, 1) <>
      String.slice(str, 25, 1) <>
      String.slice(str, 29, 1) <>
      String.slice(str, 33, 1)
  end

  def string_rotate(l, idx) do
    l |> Enum.reduce("", fn x, acc -> acc <> String.slice(x, idx, 1) end)
  end

  def parse_file(ftext) do
    ftext
    |> String.split("\n\n", trim: true)
    |> Enum.map(&String.split(&1, "\n", trim: true))
  end

  def assemble_stack(idx, parsed_stack) do
    parsed_stack
    |> Enum.reduce("", fn x, acc -> (String.slice(x, idx, 1) <> acc) |> String.trim() end)
  end

  def output_status(stacks) do
    Enum.each(stacks, fn x -> IO.puts(x) end)
    stacks
  end

  def make_move(unparsed_move, stacks) do
    IO.puts(unparsed_move)
    move = parse_move(unparsed_move)
    stack = Enum.at(stacks, move.from_stack)

    boxes =
      String.slice(stack, (String.length(stack) - move.num_crates)..String.length(stack))
      |> String.reverse()

    stacks
    |> replace_at(move.to_stack, Enum.at(stacks, move.to_stack) <> boxes)
    |> replace_at(
      move.from_stack,
      String.slice(
        Enum.at(stacks, move.from_stack),
        0..(String.length(Enum.at(stacks, move.from_stack)) - String.length(boxes) - 1)
      )
    )
    |> output_status()
  end

  def replace_at(l, idx, repl_value) do
    l
    |> Enum.with_index()
    |> Enum.map(fn
      {_, ^idx} -> repl_value
      {value, _} -> value
    end)
  end

  def parse_move(move) do
    move_elements = String.split(move)

    %{
      :num_crates => String.to_integer(Enum.at(move_elements, 1)),
      :from_stack => String.to_integer(Enum.at(move_elements, 3)) - 1,
      :to_stack => String.to_integer(Enum.at(move_elements, 5)) - 1
    }
  end
end

[raw_stack, moves] =
  Aoc.read_file("advent2022-datasets/orc-05-01-dataset.txt")
  |> Aoc.parse_file()

parsed_stack = Enum.map(raw_stack, fn x -> Aoc.parse_stack(x) end)
tail = Enum.map(0..length(parsed_stack), fn x -> Aoc.assemble_stack(x, parsed_stack) end)
IO.puts(tail)

moves
|> Enum.reduce(tail, fn x, acc -> Aoc.make_move(x, acc) end)
|> Enum.reduce("", fn x, acc -> acc <> String.slice(String.reverse(x), 0, 1) end)
```

<!-- livebook:{"output":true} -->

```
...
2TDMSBBQWNWNLNLGHDP
3C
4
5PQRJ
6
7WGTD
8NDZNLHMTWBWGCHNGTRVHNMSCP
9

move 1 from 3 to 7
1JPLW
2TDMSBBQWNWNLNLGHDP
3
4
5PQRJ
6
7WGTDC
8NDZNLHMTWBWGCHNGTRVHNMSCP
9

move 24 from 8 to 6
1JPLW
2TDMSBBQWNWNLNLGHDP
3
4
5PQRJ
6DZNLHMTWBWGCHNGTRVHNMSCP
7WGTDC
8N
9

move 3 from 6 to 5
1JPLW
2TDMSBBQWNWNLNLGHDP
3
4
5PQRJSCP
6DZNLHMTWBWGCHNGTRVHNM
7WGTDC
8N
9

move 4 from 6 to 7
1JPLW
2TDMSBBQWNWNLNLGHDP
3
4
5PQRJSCP
6DZNLHMTWBWGCHNGTR
7WGTDCVHNM
8N
9

move 1 from 1 to 7
1JPL
2TDMSBBQWNWNLNLGHDP
3
4
5PQRJSCP
6DZNLHMTWBWGCHNGTR
7WGTDCVHNMW
8N
9

move 7 from 7 to 6
1JPL
2TDMSBBQWNWNLNLGHDP
3
4
5PQRJSCP
6DZNLHMTWBWGCHNGTRDCVHNMW
7WGT
8N
9

move 7 from 5 to 3
1JPL
2TDMSBBQWNWNLNLGHDP
3PQRJSCP
4
5
6DZNLHMTWBWGCHNGTRDCVHNMW
7WGT
8N
9

move 13 from 6 to 8
1JPL
2TDMSBBQWNWNLNLGHDP
3PQRJSCP
4
5
6DZNLHMTWBWG
7WGT
8NCHNGTRDCVHNMW
9

move 3 from 1 to 2
1
2TDMSBBQWNWNLNLGHDPJPL
3PQRJSCP
4
5
6DZNLHMTWBWG
7WGT
8NCHNGTRDCVHNMW
9

move 7 from 6 to 3
1
2TDMSBBQWNWNLNLGHDPJPL
3PQRJSCPHMTWBWG
4
5
6DZNL
7WGT
8NCHNGTRDCVHNMW
9

move 12 from 2 to 4
1
2TDMSBBQWN
3PQRJSCPHMTWBWG
4WNLNLGHDPJPL
5
6DZNL
7WGT
8NCHNGTRDCVHNMW
9

move 4 from 6 to 9
1
2TDMSBBQWN
3PQRJSCPHMTWBWG
4WNLNLGHDPJPL
5
6
7WGT
8NCHNGTRDCVHNMW
9DZNL

move 6 from 3 to 1
1MTWBWG
2TDMSBBQWN
3PQRJSCPH
4WNLNLGHDPJPL
5
6
7WGT
8NCHNGTRDCVHNMW
9DZNL

move 1 from 2 to 4
1MTWBWG
2TDMSBBQW
3PQRJSCPH
4WNLNLGHDPJPLN
5
6
7WGT
8NCHNGTRDCVHNMW
9DZNL

move 2 from 8 to 7
1MTWBWG
2TDMSBBQW
3PQRJSCPH
4WNLNLGHDPJPLN
5
6
7WGTMW
8NCHNGTRDCVHN
9DZNL

move 2 from 2 to 9
1MTWBWG
2TDMSBB
3PQRJSCPH
4WNLNLGHDPJPLN
5
6
7WGTMW
8NCHNGTRDCVHN
9DZNLQW

move 6 from 3 to 4
1MTWBWG
2TDMSBB
3PQ
4WNLNLGHDPJPLNRJSCPH
5
6
7WGTMW
8NCHNGTRDCVHN
9DZNLQW

move 12 from 8 to 2
1MTWBWG
2TDMSBBNCHNGTRDCVHN
3PQ
4WNLNLGHDPJPLNRJSCPH
5
6
7WGTMW
8
9DZNLQW

move 18 from 2 to 5
1MTWBWG
2
3PQ
4WNLNLGHDPJPLNRJSCPH
5TDMSBBNCHNGTRDCVHN
6
7WGTMW
8
9DZNLQW

move 10 from 4 to 3
1MTWBWG
2
3PQJPLNRJSCPH
4WNLNLGHDP
5TDMSBBNCHNGTRDCVHN
6
7WGTMW
8
9DZNLQW

move 4 from 7 to 3
1MTWBWG
2
3PQJPLNRJSCPHGTMW
4WNLNLGHDP
5TDMSBBNCHNGTRDCVHN
6
7W
8
9DZNLQW

move 5 from 4 to 7
1MTWBWG
2
3PQJPLNRJSCPHGTMW
4WNLN
5TDMSBBNCHNGTRDCVHN
6
7WLGHDP
8
9DZNLQW

move 3 from 5 to 2
1MTWBWG
2VHN
3PQJPLNRJSCPHGTMW
4WNLN
5TDMSBBNCHNGTRDC
6
7WLGHDP
8
9DZNLQW

move 4 from 7 to 9
1MTWBWG
2VHN
3PQJPLNRJSCPHGTMW
4WNLN
5TDMSBBNCHNGTRDC
6
7WL
8
9DZNLQWGHDP

move 1 from 5 to 4
1MTWBWG
2VHN
3PQJPLNRJSCPHGTMW
4WNLNC
5TDMSBBNCHNGTRD
6
7WL
8
9DZNLQWGHDP

move 3 from 2 to 1
1MTWBWGVHN
2
3PQJPLNRJSCPHGTMW
4WNLNC
5TDMSBBNCHNGTRD
6
7WL
8
9DZNLQWGHDP

move 4 from 3 to 6
1MTWBWGVHN
2
3PQJPLNRJSCPH
4WNLNC
5TDMSBBNCHNGTRD
6GTMW
7WL
8
9DZNLQWGHDP

move 7 from 5 to 6
1MTWBWGVHN
2
3PQJPLNRJSCPH
4WNLNC
5TDMSBBN
6GTMWCHNGTRD
7WL
8
9DZNLQWGHDP

move 2 from 5 to 7
1MTWBWGVHN
2
3PQJPLNRJSCPH
4WNLNC
5TDMSB
6GTMWCHNGTRD
7WLBN
8
9DZNLQWGHDP

move 5 from 1 to 7
1MTWB
2
3PQJPLNRJSCPH
4WNLNC
5TDMSB
6GTMWCHNGTRD
7WLBNWGVHN
8
9DZNLQWGHDP

move 9 from 7 to 6
1MTWB
2
3PQJPLNRJSCPH
4WNLNC
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8
9DZNLQWGHDP

move 8 from 9 to 8
1MTWB
2
3PQJPLNRJSCPH
4WNLNC
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8NLQWGHDP
9DZ

move 1 from 1 to 3
1MTW
2
3PQJPLNRJSCPHB
4WNLNC
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8NLQWGHDP
9DZ

move 1 from 3 to 1
1MTWB
2
3PQJPLNRJSCPH
4WNLNC
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8NLQWGHDP
9DZ

move 10 from 3 to 9
1MTWB
2
3PQ
4WNLNC
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8NLQWGHDP
9DZJPLNRJSCPH

move 8 from 8 to 4
1MTWB
2
3PQ
4WNLNCNLQWGHDP
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8
9DZJPLNRJSCPH

move 1 from 3 to 8
1MTWB
2
3P
4WNLNCNLQWGHDP
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8Q
9DZJPLNRJSCPH

move 1 from 1 to 3
1MTW
2
3PB
4WNLNCNLQWGHDP
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8Q
9DZJPLNRJSCPH

move 6 from 9 to 1
1MTWRJSCPH
2
3PB
4WNLNCNLQWGHDP
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8Q
9DZJPLN

move 5 from 5 to 3
1MTWRJSCPH
2
3PBTDMSB
4WNLNCNLQWGHDP
5
6GTMWCHNGTRDWLBNWGVHN
7
8Q
9DZJPLN

move 5 from 3 to 6
1MTWRJSCPH
2
3PB
4WNLNCNLQWGHDP
5
6GTMWCHNGTRDWLBNWGVHNTDMSB
7
8Q
9DZJPLN

move 1 from 8 to 9
1MTWRJSCPH
2
3PB
4WNLNCNLQWGHDP
5
6GTMWCHNGTRDWLBNWGVHNTDMSB
7
8
9DZJPLNQ

move 19 from 6 to 2
1MTWRJSCPH
2NGTRDWLBNWGVHNTDMSB
3PB
4WNLNCNLQWGHDP
5
6GTMWCH
7
8
9DZJPLNQ

move 13 from 4 to 1
1MTWRJSCPHWNLNCNLQWGHDP
2NGTRDWLBNWGVHNTDMSB
3PB
4
5
6GTMWCH
7
8
9DZJPLNQ

move 4 from 1 to 5
1MTWRJSCPHWNLNCNLQW
2NGTRDWLBNWGVHNTDMSB
3PB
4
5GHDP
6GTMWCH
7
8
9DZJPLNQ

move 6 from 2 to 1
1MTWRJSCPHWNLNCNLQWNTDMSB
2NGTRDWLBNWGVH
3PB
4
5GHDP
6GTMWCH
7
8
9DZJPLNQ

move 2 from 9 to 4
1MTWRJSCPHWNLNCNLQWNTDMSB
2NGTRDWLBNWGVH
3PB
4NQ
5GHDP
6GTMWCH
7
8
9DZJPL

move 1 from 3 to 1
1MTWRJSCPHWNLNCNLQWNTDMSBB
2NGTRDWLBNWGVH
3P
4NQ
5GHDP
6GTMWCH
7
8
9DZJPL

move 9 from 2 to 3
1MTWRJSCPHWNLNCNLQWNTDMSBB
2NGTR
3PDWLBNWGVH
4NQ
5GHDP
6GTMWCH
7
8
9DZJPL

move 4 from 5 to 1
1MTWRJSCPHWNLNCNLQWNTDMSBBGHDP
2NGTR
3PDWLBNWGVH
4NQ
5
6GTMWCH
7
8
9DZJPL

move 5 from 9 to 6
1MTWRJSCPHWNLNCNLQWNTDMSBBGHDP
2NGTR
3PDWLBNWGVH
4NQ
5
6GTMWCHDZJPL
7
8
9

move 4 from 3 to 4
1MTWRJSCPHWNLNCNLQWNTDMSBBGHDP
2NGTR
3PDWLBN
4NQWGVH
5
6GTMWCHDZJPL
7
8
9

move 3 from 2 to 7
1MTWRJSCPHWNLNCNLQWNTDMSBBGHDP
2N
3PDWLBN
4NQWGVH
5
6GTMWCHDZJPL
7GTR
8
9

move 2 from 4 to 8
1MTWRJSCPHWNLNCNLQWNTDMSBBGHDP
2N
3PDWLBN
4NQWG
5
6GTMWCHDZJPL
7GTR
8VH
9

move 6 from 1 to 9
1MTWRJSCPHWNLNCNLQWNTDMS
2N
3PDWLBN
4NQWG
5
6GTMWCHDZJPL
7GTR
8VH
9BBGHDP

move 1 from 8 to 6
1MTWRJSCPHWNLNCNLQWNTDMS
2N
3PDWLBN
4NQWG
5
6GTMWCHDZJPLH
7GTR
8V
9BBGHDP

move 4 from 1 to 5
1MTWRJSCPHWNLNCNLQWN
2N
3PDWLBN
4NQWG
5TDMS
6GTMWCHDZJPLH
7GTR
8V
9BBGHDP

move 3 from 4 to 5
1MTWRJSCPHWNLNCNLQWN
2N
3PDWLBN
4N
5TDMSQWG
6GTMWCHDZJPLH
7GTR
8V
9BBGHDP

move 1 from 7 to 2
1MTWRJSCPHWNLNCNLQWN
2NR
3PDWLBN
4N
5TDMSQWG
6GTMWCHDZJPLH
7GT
8V
9BBGHDP

move 11 from 1 to 6
1MTWRJSCP
2NR
3PDWLBN
4N
5TDMSQWG
6GTMWCHDZJPLHHWNLNCNLQWN
7GT
8V
9BBGHDP

move 1 from 2 to 7
1MTWRJSCP
2N
3PDWLBN
4N
5TDMSQWG
6GTMWCHDZJPLHHWNLNCNLQWN
7GTR
8V
9BBGHDP

move 5 from 3 to 7
1MTWRJSCP
2N
3P
4N
5TDMSQWG
6GTMWCHDZJPLHHWNLNCNLQWN
7GTRDWLBN
8V
9BBGHDP

move 1 from 3 to 4
1MTWRJSCP
2N
3
4NP
5TDMSQWG
6GTMWCHDZJPLHHWNLNCNLQWN
7GTRDWLBN
8V
9BBGHDP

move 1 from 4 to 8
1MTWRJSCP
2N
3
4N
5TDMSQWG
6GTMWCHDZJPLHHWNLNCNLQWN
7GTRDWLBN
8VP
9BBGHDP

move 3 from 5 to 6
1MTWRJSCP
2N
3
4N
5TDMS
6GTMWCHDZJPLHHWNLNCNLQWNQWG
7GTRDWLBN
8VP
9BBGHDP

move 8 from 1 to 7
1
2N
3
4N
5TDMS
6GTMWCHDZJPLHHWNLNCNLQWNQWG
7GTRDWLBNMTWRJSCP
8VP
9BBGHDP

move 1 from 8 to 9
1
2N
3
4N
5TDMS
6GTMWCHDZJPLHHWNLNCNLQWNQWG
7GTRDWLBNMTWRJSCP
8V
9BBGHDPP

move 1 from 6 to 9
1
2N
3
4N
5TDMS
6GTMWCHDZJPLHHWNLNCNLQWNQW
7GTRDWLBNMTWRJSCP
8V
9BBGHDPPG

move 1 from 8 to 5
1
2N
3
4N
5TDMSV
6GTMWCHDZJPLHHWNLNCNLQWNQW
7GTRDWLBNMTWRJSCP
8
9BBGHDPPG

move 11 from 6 to 5
1
2N
3
4N
5TDMSVNLNCNLQWNQW
6GTMWCHDZJPLHHW
7GTRDWLBNMTWRJSCP
8
9BBGHDPPG

move 12 from 5 to 2
1
2NVNLNCNLQWNQW
3
4N
5TDMS
6GTMWCHDZJPLHHW
7GTRDWLBNMTWRJSCP
8
9BBGHDPPG

move 1 from 5 to 2
1
2NVNLNCNLQWNQWS
3
4N
5TDM
6GTMWCHDZJPLHHW
7GTRDWLBNMTWRJSCP
8
9BBGHDPPG

move 8 from 7 to 3
1
2NVNLNCNLQWNQWS
3MTWRJSCP
4N
5TDM
6GTMWCHDZJPLHHW
7GTRDWLBN
8
9BBGHDPPG

move 1 from 5 to 6
1
2NVNLNCNLQWNQWS
3MTWRJSCP
4N
5TD
6GTMWCHDZJPLHHWM
7GTRDWLBN
8
9BBGHDPPG

move 2 from 5 to 6
1
2NVNLNCNLQWNQWS
3MTWRJSCP
4N
5
6GTMWCHDZJPLHHWMTD
7GTRDWLBN
8
9BBGHDPPG

move 3 from 7 to 1
1LBN
2NVNLNCNLQWNQWS
3MTWRJSCP
4N
5
6GTMWCHDZJPLHHWMTD
7GTRDW
8
9BBGHDPPG

move 6 from 2 to 6
1LBN
2NVNLNCNL
3MTWRJSCP
4N
5
6GTMWCHDZJPLHHWMTDQWNQWS
7GTRDW
8
9BBGHDPPG

move 1 from 3 to 1
1LBNP
2NVNLNCNL
3MTWRJSC
4N
5
6GTMWCHDZJPLHHWMTDQWNQWS
7GTRDW
8
9BBGHDPPG

move 1 from 4 to 1
1LBNPN
2NVNLNCNL
3MTWRJSC
4
5
6GTMWCHDZJPLHHWMTDQWNQWS
7GTRDW
8
9BBGHDPPG

move 4 from 6 to 2
1LBNPN
2NVNLNCNLNQWS
3MTWRJSC
4
5
6GTMWCHDZJPLHHWMTDQW
7GTRDW
8
9BBGHDPPG

move 5 from 1 to 5
1
2NVNLNCNLNQWS
3MTWRJSC
4
5LBNPN
6GTMWCHDZJPLHHWMTDQW
7GTRDW
8
9BBGHDPPG

move 10 from 2 to 3
1
2NV
3MTWRJSCNLNCNLNQWS
4
5LBNPN
6GTMWCHDZJPLHHWMTDQW
7GTRDW
8
9BBGHDPPG

move 2 from 9 to 4
1
2NV
3MTWRJSCNLNCNLNQWS
4PG
5LBNPN
6GTMWCHDZJPLHHWMTDQW
7GTRDW
8
9BBGHDP

move 4 from 5 to 8
1
2NV
3MTWRJSCNLNCNLNQWS
4PG
5L
6GTMWCHDZJPLHHWMTDQW
7GTRDW
8BNPN
9BBGHDP

move 2 from 2 to 7
1
2
3MTWRJSCNLNCNLNQWS
4PG
5L
6GTMWCHDZJPLHHWMTDQW
7GTRDWNV
8BNPN
9BBGHDP

move 12 from 6 to 7
1
2
3MTWRJSCNLNCNLNQWS
4PG
5L
6GTMWCHD
7GTRDWNVZJPLHHWMTDQW
8BNPN
9BBGHDP

move 1 from 8 to 2
1
2N
3MTWRJSCNLNCNLNQWS
4PG
5L
6GTMWCHD
7GTRDWNVZJPLHHWMTDQW
8BNP
9BBGHDP

move 10 from 3 to 4
1
2N
3MTWRJSC
4PGNLNCNLNQWS
5L
6GTMWCHD
7GTRDWNVZJPLHHWMTDQW
8BNP
9BBGHDP

move 2 from 3 to 5
1
2N
3MTWRJ
4PGNLNCNLNQWS
5LSC
6GTMWCHD
7GTRDWNVZJPLHHWMTDQW
8BNP
9BBGHDP

move 1 from 3 to 1
1J
2N
3MTWR
4PGNLNCNLNQWS
5LSC
6GTMWCHD
7GTRDWNVZJPLHHWMTDQW
8BNP
9BBGHDP

```

<!-- livebook:{"output":true} -->

```
"JNRSCDWPP"
```

## Day 5 Part 2

Note this identical to Day 5 Part 1 except we don't reverse the order of the boxes when they are moved from one stack to another.  Specifically, line 56  (the `|> String.reverse()`) is removed from the solution above.

```elixir
defmodule Aoc do
  def read_file(fname) do
    {:ok, ftext} = File.read(fname)
    ftext
  end

  def create_ranges(line) do
    elves = String.split(line, ",")

    [assignment1, assignment2] =
      String.split(Enum.at(elves, 0), "-")
      |> Enum.map(&String.to_integer(&1))

    [assignment3, assignment4] =
      String.split(Enum.at(elves, 1), "-")
      |> Enum.map(&String.to_integer(&1))

    elf1 = assignment1..assignment2
    elf2 = assignment3..assignment4

    cond do
      assignment1 in elf2 or assignment2 in elf2 ->
        1

      assignment3 in elf1 or assignment4 in elf1 ->
        1

      true ->
        0
    end
  end

  def parse_stack(str) do
    String.slice(str, 1, 1) <>
      String.slice(str, 5, 1) <>
      String.slice(str, 9, 1) <>
      String.slice(str, 13, 1) <>
      String.slice(str, 17, 1) <>
      String.slice(str, 21, 1) <>
      String.slice(str, 25, 1) <>
      String.slice(str, 29, 1) <>
      String.slice(str, 33, 1)
  end

  def string_rotate(l, idx) do
    l |> Enum.reduce("", fn x, acc -> acc <> String.slice(x, idx, 1) end)
  end

  def parse_file(ftext) do
    ftext
    |> String.split("\n\n", trim: true)
    |> Enum.map(&String.split(&1, "\n", trim: true))
  end

  def assemble_stack(idx, parsed_stack) do
    parsed_stack
    |> Enum.reduce("", fn x, acc -> (String.slice(x, idx, 1) <> acc) |> String.trim() end)
  end

  def output_status(stacks) do
    Enum.each(stacks, fn x -> IO.puts(x) end)
    stacks
  end

  def make_move(unparsed_move, stacks) do
    IO.puts(unparsed_move)
    move = parse_move(unparsed_move)
    stack = Enum.at(stacks, move.from_stack)
    boxes = String.slice(stack, (String.length(stack) - move.num_crates)..String.length(stack))

    stacks
    |> replace_at(move.to_stack, Enum.at(stacks, move.to_stack) <> boxes)
    |> replace_at(
      move.from_stack,
      String.slice(
        Enum.at(stacks, move.from_stack),
        0..(String.length(Enum.at(stacks, move.from_stack)) - String.length(boxes) - 1)
      )
    )
    |> output_status()
  end

  def replace_at(l, idx, repl_value) do
    l
    |> Enum.with_index()
    |> Enum.map(fn
      {_, ^idx} -> repl_value
      {value, _} -> value
    end)
  end

  def parse_move(move) do
    move_elements = String.split(move)

    %{
      :num_crates => String.to_integer(Enum.at(move_elements, 1)),
      :from_stack => String.to_integer(Enum.at(move_elements, 3)) - 1,
      :to_stack => String.to_integer(Enum.at(move_elements, 5)) - 1
    }
  end
end

[raw_stack, moves] =
  Aoc.read_file("advent2022-datasets/orc-05-01-dataset.txt")
  |> Aoc.parse_file()

parsed_stack = Enum.map(raw_stack, fn x -> Aoc.parse_stack(x) end)
tail = Enum.map(0..length(parsed_stack), fn x -> Aoc.assemble_stack(x, parsed_stack) end)
IO.puts(tail)

moves
|> Enum.reduce(tail, fn x, acc -> Aoc.make_move(x, acc) end)
|> Enum.reduce("", fn x, acc -> acc <> String.slice(String.reverse(x), 0, 1) end)
```

<!-- livebook:{"output":true} -->

```
...
2TDMSBBQWNWNLNLGHDP
3C
4
5PQRJ
6
7WGTD
8NDZNLHMTWBWGCHNGTRVHNMSCP
9

move 1 from 3 to 7
1JPLW
2TDMSBBQWNWNLNLGHDP
3
4
5PQRJ
6
7WGTDC
8NDZNLHMTWBWGCHNGTRVHNMSCP
9

move 24 from 8 to 6
1JPLW
2TDMSBBQWNWNLNLGHDP
3
4
5PQRJ
6DZNLHMTWBWGCHNGTRVHNMSCP
7WGTDC
8N
9

move 3 from 6 to 5
1JPLW
2TDMSBBQWNWNLNLGHDP
3
4
5PQRJSCP
6DZNLHMTWBWGCHNGTRVHNM
7WGTDC
8N
9

move 4 from 6 to 7
1JPLW
2TDMSBBQWNWNLNLGHDP
3
4
5PQRJSCP
6DZNLHMTWBWGCHNGTR
7WGTDCVHNM
8N
9

move 1 from 1 to 7
1JPL
2TDMSBBQWNWNLNLGHDP
3
4
5PQRJSCP
6DZNLHMTWBWGCHNGTR
7WGTDCVHNMW
8N
9

move 7 from 7 to 6
1JPL
2TDMSBBQWNWNLNLGHDP
3
4
5PQRJSCP
6DZNLHMTWBWGCHNGTRDCVHNMW
7WGT
8N
9

move 7 from 5 to 3
1JPL
2TDMSBBQWNWNLNLGHDP
3PQRJSCP
4
5
6DZNLHMTWBWGCHNGTRDCVHNMW
7WGT
8N
9

move 13 from 6 to 8
1JPL
2TDMSBBQWNWNLNLGHDP
3PQRJSCP
4
5
6DZNLHMTWBWG
7WGT
8NCHNGTRDCVHNMW
9

move 3 from 1 to 2
1
2TDMSBBQWNWNLNLGHDPJPL
3PQRJSCP
4
5
6DZNLHMTWBWG
7WGT
8NCHNGTRDCVHNMW
9

move 7 from 6 to 3
1
2TDMSBBQWNWNLNLGHDPJPL
3PQRJSCPHMTWBWG
4
5
6DZNL
7WGT
8NCHNGTRDCVHNMW
9

move 12 from 2 to 4
1
2TDMSBBQWN
3PQRJSCPHMTWBWG
4WNLNLGHDPJPL
5
6DZNL
7WGT
8NCHNGTRDCVHNMW
9

move 4 from 6 to 9
1
2TDMSBBQWN
3PQRJSCPHMTWBWG
4WNLNLGHDPJPL
5
6
7WGT
8NCHNGTRDCVHNMW
9DZNL

move 6 from 3 to 1
1MTWBWG
2TDMSBBQWN
3PQRJSCPH
4WNLNLGHDPJPL
5
6
7WGT
8NCHNGTRDCVHNMW
9DZNL

move 1 from 2 to 4
1MTWBWG
2TDMSBBQW
3PQRJSCPH
4WNLNLGHDPJPLN
5
6
7WGT
8NCHNGTRDCVHNMW
9DZNL

move 2 from 8 to 7
1MTWBWG
2TDMSBBQW
3PQRJSCPH
4WNLNLGHDPJPLN
5
6
7WGTMW
8NCHNGTRDCVHN
9DZNL

move 2 from 2 to 9
1MTWBWG
2TDMSBB
3PQRJSCPH
4WNLNLGHDPJPLN
5
6
7WGTMW
8NCHNGTRDCVHN
9DZNLQW

move 6 from 3 to 4
1MTWBWG
2TDMSBB
3PQ
4WNLNLGHDPJPLNRJSCPH
5
6
7WGTMW
8NCHNGTRDCVHN
9DZNLQW

move 12 from 8 to 2
1MTWBWG
2TDMSBBNCHNGTRDCVHN
3PQ
4WNLNLGHDPJPLNRJSCPH
5
6
7WGTMW
8
9DZNLQW

move 18 from 2 to 5
1MTWBWG
2
3PQ
4WNLNLGHDPJPLNRJSCPH
5TDMSBBNCHNGTRDCVHN
6
7WGTMW
8
9DZNLQW

move 10 from 4 to 3
1MTWBWG
2
3PQJPLNRJSCPH
4WNLNLGHDP
5TDMSBBNCHNGTRDCVHN
6
7WGTMW
8
9DZNLQW

move 4 from 7 to 3
1MTWBWG
2
3PQJPLNRJSCPHGTMW
4WNLNLGHDP
5TDMSBBNCHNGTRDCVHN
6
7W
8
9DZNLQW

move 5 from 4 to 7
1MTWBWG
2
3PQJPLNRJSCPHGTMW
4WNLN
5TDMSBBNCHNGTRDCVHN
6
7WLGHDP
8
9DZNLQW

move 3 from 5 to 2
1MTWBWG
2VHN
3PQJPLNRJSCPHGTMW
4WNLN
5TDMSBBNCHNGTRDC
6
7WLGHDP
8
9DZNLQW

move 4 from 7 to 9
1MTWBWG
2VHN
3PQJPLNRJSCPHGTMW
4WNLN
5TDMSBBNCHNGTRDC
6
7WL
8
9DZNLQWGHDP

move 1 from 5 to 4
1MTWBWG
2VHN
3PQJPLNRJSCPHGTMW
4WNLNC
5TDMSBBNCHNGTRD
6
7WL
8
9DZNLQWGHDP

move 3 from 2 to 1
1MTWBWGVHN
2
3PQJPLNRJSCPHGTMW
4WNLNC
5TDMSBBNCHNGTRD
6
7WL
8
9DZNLQWGHDP

move 4 from 3 to 6
1MTWBWGVHN
2
3PQJPLNRJSCPH
4WNLNC
5TDMSBBNCHNGTRD
6GTMW
7WL
8
9DZNLQWGHDP

move 7 from 5 to 6
1MTWBWGVHN
2
3PQJPLNRJSCPH
4WNLNC
5TDMSBBN
6GTMWCHNGTRD
7WL
8
9DZNLQWGHDP

move 2 from 5 to 7
1MTWBWGVHN
2
3PQJPLNRJSCPH
4WNLNC
5TDMSB
6GTMWCHNGTRD
7WLBN
8
9DZNLQWGHDP

move 5 from 1 to 7
1MTWB
2
3PQJPLNRJSCPH
4WNLNC
5TDMSB
6GTMWCHNGTRD
7WLBNWGVHN
8
9DZNLQWGHDP

move 9 from 7 to 6
1MTWB
2
3PQJPLNRJSCPH
4WNLNC
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8
9DZNLQWGHDP

move 8 from 9 to 8
1MTWB
2
3PQJPLNRJSCPH
4WNLNC
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8NLQWGHDP
9DZ

move 1 from 1 to 3
1MTW
2
3PQJPLNRJSCPHB
4WNLNC
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8NLQWGHDP
9DZ

move 1 from 3 to 1
1MTWB
2
3PQJPLNRJSCPH
4WNLNC
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8NLQWGHDP
9DZ

move 10 from 3 to 9
1MTWB
2
3PQ
4WNLNC
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8NLQWGHDP
9DZJPLNRJSCPH

move 8 from 8 to 4
1MTWB
2
3PQ
4WNLNCNLQWGHDP
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8
9DZJPLNRJSCPH

move 1 from 3 to 8
1MTWB
2
3P
4WNLNCNLQWGHDP
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8Q
9DZJPLNRJSCPH

move 1 from 1 to 3
1MTW
2
3PB
4WNLNCNLQWGHDP
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8Q
9DZJPLNRJSCPH

move 6 from 9 to 1
1MTWRJSCPH
2
3PB
4WNLNCNLQWGHDP
5TDMSB
6GTMWCHNGTRDWLBNWGVHN
7
8Q
9DZJPLN

move 5 from 5 to 3
1MTWRJSCPH
2
3PBTDMSB
4WNLNCNLQWGHDP
5
6GTMWCHNGTRDWLBNWGVHN
7
8Q
9DZJPLN

move 5 from 3 to 6
1MTWRJSCPH
2
3PB
4WNLNCNLQWGHDP
5
6GTMWCHNGTRDWLBNWGVHNTDMSB
7
8Q
9DZJPLN

move 1 from 8 to 9
1MTWRJSCPH
2
3PB
4WNLNCNLQWGHDP
5
6GTMWCHNGTRDWLBNWGVHNTDMSB
7
8
9DZJPLNQ

move 19 from 6 to 2
1MTWRJSCPH
2NGTRDWLBNWGVHNTDMSB
3PB
4WNLNCNLQWGHDP
5
6GTMWCH
7
8
9DZJPLNQ

move 13 from 4 to 1
1MTWRJSCPHWNLNCNLQWGHDP
2NGTRDWLBNWGVHNTDMSB
3PB
4
5
6GTMWCH
7
8
9DZJPLNQ

move 4 from 1 to 5
1MTWRJSCPHWNLNCNLQW
2NGTRDWLBNWGVHNTDMSB
3PB
4
5GHDP
6GTMWCH
7
8
9DZJPLNQ

move 6 from 2 to 1
1MTWRJSCPHWNLNCNLQWNTDMSB
2NGTRDWLBNWGVH
3PB
4
5GHDP
6GTMWCH
7
8
9DZJPLNQ

move 2 from 9 to 4
1MTWRJSCPHWNLNCNLQWNTDMSB
2NGTRDWLBNWGVH
3PB
4NQ
5GHDP
6GTMWCH
7
8
9DZJPL

move 1 from 3 to 1
1MTWRJSCPHWNLNCNLQWNTDMSBB
2NGTRDWLBNWGVH
3P
4NQ
5GHDP
6GTMWCH
7
8
9DZJPL

move 9 from 2 to 3
1MTWRJSCPHWNLNCNLQWNTDMSBB
2NGTR
3PDWLBNWGVH
4NQ
5GHDP
6GTMWCH
7
8
9DZJPL

move 4 from 5 to 1
1MTWRJSCPHWNLNCNLQWNTDMSBBGHDP
2NGTR
3PDWLBNWGVH
4NQ
5
6GTMWCH
7
8
9DZJPL

move 5 from 9 to 6
1MTWRJSCPHWNLNCNLQWNTDMSBBGHDP
2NGTR
3PDWLBNWGVH
4NQ
5
6GTMWCHDZJPL
7
8
9

move 4 from 3 to 4
1MTWRJSCPHWNLNCNLQWNTDMSBBGHDP
2NGTR
3PDWLBN
4NQWGVH
5
6GTMWCHDZJPL
7
8
9

move 3 from 2 to 7
1MTWRJSCPHWNLNCNLQWNTDMSBBGHDP
2N
3PDWLBN
4NQWGVH
5
6GTMWCHDZJPL
7GTR
8
9

move 2 from 4 to 8
1MTWRJSCPHWNLNCNLQWNTDMSBBGHDP
2N
3PDWLBN
4NQWG
5
6GTMWCHDZJPL
7GTR
8VH
9

move 6 from 1 to 9
1MTWRJSCPHWNLNCNLQWNTDMS
2N
3PDWLBN
4NQWG
5
6GTMWCHDZJPL
7GTR
8VH
9BBGHDP

move 1 from 8 to 6
1MTWRJSCPHWNLNCNLQWNTDMS
2N
3PDWLBN
4NQWG
5
6GTMWCHDZJPLH
7GTR
8V
9BBGHDP

move 4 from 1 to 5
1MTWRJSCPHWNLNCNLQWN
2N
3PDWLBN
4NQWG
5TDMS
6GTMWCHDZJPLH
7GTR
8V
9BBGHDP

move 3 from 4 to 5
1MTWRJSCPHWNLNCNLQWN
2N
3PDWLBN
4N
5TDMSQWG
6GTMWCHDZJPLH
7GTR
8V
9BBGHDP

move 1 from 7 to 2
1MTWRJSCPHWNLNCNLQWN
2NR
3PDWLBN
4N
5TDMSQWG
6GTMWCHDZJPLH
7GT
8V
9BBGHDP

move 11 from 1 to 6
1MTWRJSCP
2NR
3PDWLBN
4N
5TDMSQWG
6GTMWCHDZJPLHHWNLNCNLQWN
7GT
8V
9BBGHDP

move 1 from 2 to 7
1MTWRJSCP
2N
3PDWLBN
4N
5TDMSQWG
6GTMWCHDZJPLHHWNLNCNLQWN
7GTR
8V
9BBGHDP

move 5 from 3 to 7
1MTWRJSCP
2N
3P
4N
5TDMSQWG
6GTMWCHDZJPLHHWNLNCNLQWN
7GTRDWLBN
8V
9BBGHDP

move 1 from 3 to 4
1MTWRJSCP
2N
3
4NP
5TDMSQWG
6GTMWCHDZJPLHHWNLNCNLQWN
7GTRDWLBN
8V
9BBGHDP

move 1 from 4 to 8
1MTWRJSCP
2N
3
4N
5TDMSQWG
6GTMWCHDZJPLHHWNLNCNLQWN
7GTRDWLBN
8VP
9BBGHDP

move 3 from 5 to 6
1MTWRJSCP
2N
3
4N
5TDMS
6GTMWCHDZJPLHHWNLNCNLQWNQWG
7GTRDWLBN
8VP
9BBGHDP

move 8 from 1 to 7
1
2N
3
4N
5TDMS
6GTMWCHDZJPLHHWNLNCNLQWNQWG
7GTRDWLBNMTWRJSCP
8VP
9BBGHDP

move 1 from 8 to 9
1
2N
3
4N
5TDMS
6GTMWCHDZJPLHHWNLNCNLQWNQWG
7GTRDWLBNMTWRJSCP
8V
9BBGHDPP

move 1 from 6 to 9
1
2N
3
4N
5TDMS
6GTMWCHDZJPLHHWNLNCNLQWNQW
7GTRDWLBNMTWRJSCP
8V
9BBGHDPPG

move 1 from 8 to 5
1
2N
3
4N
5TDMSV
6GTMWCHDZJPLHHWNLNCNLQWNQW
7GTRDWLBNMTWRJSCP
8
9BBGHDPPG

move 11 from 6 to 5
1
2N
3
4N
5TDMSVNLNCNLQWNQW
6GTMWCHDZJPLHHW
7GTRDWLBNMTWRJSCP
8
9BBGHDPPG

move 12 from 5 to 2
1
2NVNLNCNLQWNQW
3
4N
5TDMS
6GTMWCHDZJPLHHW
7GTRDWLBNMTWRJSCP
8
9BBGHDPPG

move 1 from 5 to 2
1
2NVNLNCNLQWNQWS
3
4N
5TDM
6GTMWCHDZJPLHHW
7GTRDWLBNMTWRJSCP
8
9BBGHDPPG

move 8 from 7 to 3
1
2NVNLNCNLQWNQWS
3MTWRJSCP
4N
5TDM
6GTMWCHDZJPLHHW
7GTRDWLBN
8
9BBGHDPPG

move 1 from 5 to 6
1
2NVNLNCNLQWNQWS
3MTWRJSCP
4N
5TD
6GTMWCHDZJPLHHWM
7GTRDWLBN
8
9BBGHDPPG

move 2 from 5 to 6
1
2NVNLNCNLQWNQWS
3MTWRJSCP
4N
5
6GTMWCHDZJPLHHWMTD
7GTRDWLBN
8
9BBGHDPPG

move 3 from 7 to 1
1LBN
2NVNLNCNLQWNQWS
3MTWRJSCP
4N
5
6GTMWCHDZJPLHHWMTD
7GTRDW
8
9BBGHDPPG

move 6 from 2 to 6
1LBN
2NVNLNCNL
3MTWRJSCP
4N
5
6GTMWCHDZJPLHHWMTDQWNQWS
7GTRDW
8
9BBGHDPPG

move 1 from 3 to 1
1LBNP
2NVNLNCNL
3MTWRJSC
4N
5
6GTMWCHDZJPLHHWMTDQWNQWS
7GTRDW
8
9BBGHDPPG

move 1 from 4 to 1
1LBNPN
2NVNLNCNL
3MTWRJSC
4
5
6GTMWCHDZJPLHHWMTDQWNQWS
7GTRDW
8
9BBGHDPPG

move 4 from 6 to 2
1LBNPN
2NVNLNCNLNQWS
3MTWRJSC
4
5
6GTMWCHDZJPLHHWMTDQW
7GTRDW
8
9BBGHDPPG

move 5 from 1 to 5
1
2NVNLNCNLNQWS
3MTWRJSC
4
5LBNPN
6GTMWCHDZJPLHHWMTDQW
7GTRDW
8
9BBGHDPPG

move 10 from 2 to 3
1
2NV
3MTWRJSCNLNCNLNQWS
4
5LBNPN
6GTMWCHDZJPLHHWMTDQW
7GTRDW
8
9BBGHDPPG

move 2 from 9 to 4
1
2NV
3MTWRJSCNLNCNLNQWS
4PG
5LBNPN
6GTMWCHDZJPLHHWMTDQW
7GTRDW
8
9BBGHDP

move 4 from 5 to 8
1
2NV
3MTWRJSCNLNCNLNQWS
4PG
5L
6GTMWCHDZJPLHHWMTDQW
7GTRDW
8BNPN
9BBGHDP

move 2 from 2 to 7
1
2
3MTWRJSCNLNCNLNQWS
4PG
5L
6GTMWCHDZJPLHHWMTDQW
7GTRDWNV
8BNPN
9BBGHDP

move 12 from 6 to 7
1
2
3MTWRJSCNLNCNLNQWS
4PG
5L
6GTMWCHD
7GTRDWNVZJPLHHWMTDQW
8BNPN
9BBGHDP

move 1 from 8 to 2
1
2N
3MTWRJSCNLNCNLNQWS
4PG
5L
6GTMWCHD
7GTRDWNVZJPLHHWMTDQW
8BNP
9BBGHDP

move 10 from 3 to 4
1
2N
3MTWRJSC
4PGNLNCNLNQWS
5L
6GTMWCHD
7GTRDWNVZJPLHHWMTDQW
8BNP
9BBGHDP

move 2 from 3 to 5
1
2N
3MTWRJ
4PGNLNCNLNQWS
5LSC
6GTMWCHD
7GTRDWNVZJPLHHWMTDQW
8BNP
9BBGHDP

move 1 from 3 to 1
1J
2N
3MTWR
4PGNLNCNLNQWS
5LSC
6GTMWCHD
7GTRDWNVZJPLHHWMTDQW
8BNP
9BBGHDP

```

<!-- livebook:{"output":true} -->

```
"JNRSCDWPP"
```

## Day 6 Part 1

```elixir
defmodule Aoc do
  def read_file(fname) do
    {:ok, ftext} = File.read(fname)
    ftext
  end

  def parse_file(ftext) do
    ftext |> String.split("\n", trim: true)
  end

  def chars_to_list(ftext) do
    ftext |> Enum.map(fn x -> String.split(x, "", trim: true) end)
  end

  def dup_status(charmap, character) do
    if Map.has_key?(charmap, character) do
      charmap[character] + 1
    else
      1
    end
  end

  def duplicates(segment) do
    num_values =
      segment
      |> Enum.reduce(%{}, fn x, acc -> Map.put(acc, x, dup_status(acc, x)) end)
      |> Map.values()

    cond do
      num_values == [1, 1, 1, 1] ->
        false

      true ->
        true
    end
  end

  def sliding_window(stream, term_char) do
    Enum.slice(stream, term_char - 4, 4)
  end

  def startofpacket(stream) do
    4..length(stream)
    |> Enum.reduce_while(0, fn x, acc ->
      if duplicates(sliding_window(stream, x)),
        do: {:cont, x},
        else: {:halt, acc + 1}
    end)
  end
end

Aoc.read_file("advent2022-datasets/orc-06-01-dataset.txt")
|> Aoc.parse_file()
|> Aoc.chars_to_list()
|> Enum.map(fn x -> Aoc.startofpacket(x) end)
```

<!-- livebook:{"output":true} -->

```
[1235]
```

Day 6 Part 2 is the same as Part 1 except we change the sliding window to 14 characters instead of 4.  Changes were made on lines 27, 37, and 41.

```elixir
defmodule Aoc do
  def read_file(fname) do
    {:ok, ftext} = File.read(fname)
    ftext
  end

  def parse_file(ftext) do
    ftext |> String.split("\n", trim: true)
  end

  def chars_to_list(ftext) do
    ftext |> Enum.map(fn x -> String.split(x, "", trim: true) end)
  end

  def dup_status(charmap, character) do
    if Map.has_key?(charmap, character) do
      charmap[character] + 1
    else
      1
    end
  end

  def duplicates(segment) do
    num_values =
      segment
      |> Enum.reduce(%{}, fn x, acc -> Map.put(acc, x, dup_status(acc, x)) end)
      |> Map.values()

    cond do
      num_values == [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1] ->
        false

      true ->
        true
    end
  end

  def sliding_window(stream, term_char) do
    Enum.slice(stream, term_char - 14, 14)
  end

  def startofpacket(stream) do
    14..length(stream)
    |> Enum.reduce_while(0, fn x, acc ->
      if duplicates(sliding_window(stream, x)),
        do: {:cont, x},
        else: {:halt, acc + 1}
    end)
  end
end

Aoc.read_file("advent2022-datasets/orc-06-01-dataset.txt")
|> Aoc.parse_file()
|> Aoc.chars_to_list()
|> Enum.map(fn x -> Aoc.startofpacket(x) end)
```

<!-- livebook:{"output":true} -->

```
[3051]
```
