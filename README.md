# aoc22

This is my repo for [Advent of Code 2022](https://adventofcode.com/2022).

Most of the puzzles are done with Elixir's [Livebook](https://livebook.dev), but some are actual Elixir projects.

